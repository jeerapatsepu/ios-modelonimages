//
//  ViewController.swift
//  ARTrackingImage
//
//  Created by Jeerapat Sripumngoen on 30/6/2563 BE.
//  Copyright © 2563 Jeerapat Sripumngoen. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
//        sceneView.showsStatistics = true
        
//        // Create a new scene
//        let scene = SCNScene(named: "art.scnassets/ship.scn")!
//
//        // Set the scene to the view
//        sceneView.scene = scene
        
        sceneView.autoenablesDefaultLighting = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARImageTrackingConfiguration()
        
        if let imagesToTrack = ARReferenceImage.referenceImages(inGroupNamed: "pokemon", bundle: Bundle.main) {
            
            configuration.trackingImages = imagesToTrack
            
            configuration.maximumNumberOfTrackedImages = 2
            
        }
        
        // Run the view's session
        sceneView.session.run(configuration)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    

//     Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {

        let node = SCNNode()

        if let imageAnchor = anchor as? ARImageAnchor {

            let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)

            plane.firstMaterial?.diffuse.contents = UIColor(white: 1, alpha: 0.5)

            let planeNode = SCNNode(geometry: plane)
            
            planeNode.eulerAngles.x = .pi / 2
            
            if imageAnchor.referenceImage.name == "Eevee" {
                let modelScn = SCNScene(named: "art.scnassets/BroadcasterScene.scn")

                let modelNode = modelScn?.rootNode.childNode(withName: "Broadcaster", recursively: true)
                
                modelNode?.position = SCNVector3(planeNode.position.x, planeNode.position.y, planeNode.position.z)
                
                node.addChildNode(modelNode!)
            }
            
            if imageAnchor.referenceImage.name == "Pikachu" {
                let modelScn = SCNScene(named: "art.scnassets/PikachuScene.scn")

                let modelNode = modelScn?.rootNode.childNode(withName: "Pikachu", recursively: true)

                modelNode?.position = SCNVector3(planeNode.position.x, planeNode.position.y, planeNode.position.z)

                node.addChildNode(modelNode!)
            }
            
//            node.addChildNode(planeNode)

        }

        return node
    }

    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
